var socket = io("https://click-royale.herokuapp.com/");
var game = 0 
username=prompt("Please enter a username", "")
if(!username){
    var int = getRndInteger(1000,9999)
    username = "Player"+int.toString()
}
socket.emit("newplayer",{username:username});

window.onbeforeunload = function(){
   socket.emit("quit",{game:game});
}
socket.on('heartbeat',
	function(data) {
		if(game>0&&!spectating){
			players=data.games[game].players
			for(var i=0;i<players.length;i++)if(players[i].id==socket.id){
				player=players[i]
				opponent=data.games[game].players[1-i]
				$("#hp").html(player.toString()+" Health")
				$("#enemyhp").html(player.toString()+" Health")
				for(var j=0;j<player.hand.length;j++){
					var btn = $('<button onclick="card('+j.toString()+')">')
					var img = $('<img>');
					img.attr('src', "cards/"+player.hand[j]+".png");
					img.appendTo(btn);
					btn.appendTo("#hand")
				}
				for(var j=0;j<opponent.hand.length;j++){
					var img = $('<img>');
					img.attr('src', "cards/"+opponent.hand[j]+".png");
					img.appendTo("#enemyhand")
				}
			}
		}
	}

).on('heartbeat2',
	function(data) {
	    if(game==0){
	        dotdotdot++
	        if(dotdotdot==1)$("#button").html("⬤●●")
	        if(dotdotdot==2)$("#button").html("●⬤●")
	        if(dotdotdot==3)$("#button").html("●●⬤")
	        if(dotdotdot==4)$("#button").html("●●●")
			$("#4").html("Bots ("+data.votes[1].toString()+")")
			$("#5").html("No Bots ("+data.votes[2].toString()+")")
			$("#start").html("Quickstart ("+data.votes[3].toString()+"/"+data.games[0].players.length+")")
	        if(dotdotdot==4)dotdotdot=0
	    }
		players = data.games[game].players
		$(".enemy").css("background-color:#FFAF50;")
		$("#enemies").html("<p class='display'>Enemies:</p>")
	    if(game==0)$("#enemies").html("<p class='display'>Lobby:</p>")
		$("#enemies2").html("<p class='display'>"+players.length.toString()+" Alive</p>")
		if(players.length==1&&game>0&&!spectating){
		    spectating=true;
			$("#button").html("You win!")
			for(var j=0;j<upgrades.length;j++)$("#"+j.toString()).html("Return to lobby")
		}
		if(game==0)$("#enemies2").html("<p class='display'>"+players.length.toString()+"/3</p>")
		if(game==0&&players.length>2)$("#enemies2").html("<p class='display'>"+Math.floor((data.req-players.length)*10).toString()+"</p>")
		for(var i=0;i<players.length&&i<12;i++)if(players[i].id!=socket.id){
			var r=$("<button class='enemy'></button>").html("<span title='"+players[i].username+"'>"+Math.floor(players[i].clicks).toString()+"</span>").data("id",players[i].id).prop("title",players[i].username);
			$("#enemies").append(r)
		}
		for(var i=12;i<players.length;i++)if(players[i].id!=socket.id){
			var r=$("<button class='enemy'></button>").text(Math.floor(players[i].clicks).toString()).data("id",players[i].id).prop("title",players[i].username);
			$("#enemies2").append(r)
		}
		$(".enemy").click(function(event){
			if(game>0)socket.emit("attack",{game:game,id:$(event.target).data("id")});
			
		})
	}
).on('cheat',
	function(data) {
	    if(data.cheater==socket.id)alert("Autoclick detected")
	}
).on('startgame',
	function(data) {
		if(game==0){
			game=data.game
		    var snd = new Audio("content/gamestart.wav");
			snd.play();
    	    for(var i=0;i<4;i++)$("#"+i.toString()).show();
    	    $("#winner").hide()
    	    $("#discord").hide()
			$("#start").hide()
			$("#bonus").show()
			
		}
	}
).on('attack',
	function(data) {
		if(data.attacked==socket.id){
			console.log(data)
			$(".enemy").filter(function () {
				return $(this).data("id") == data.attacker;
			}).css({backgroundColor:"#f00"});
		}
	}
);
$("#start").on("click",function(){
	socket.emit("vote",{type:3});
})
for(var i=0;i<6;i++){
	$("#"+i.toString()).on("click",function(i){
		return function(){
		if(i==4&&game==0)socket.emit("vote",{type:1});
		if(i==5&&game==0)socket.emit("vote",{type:2});
        if(spectating){
    		game=0
    	    spectating=false;
    	    socket.emit("newplayer",{username:username});
    		$("#button").html("Waiting")
    		$("#discord").show()
			$("#start").show()
			$("#bonus").hide()
    		for(var j=0;j<4;j++)$("#"+j.toString()).hide();
    	}
		if(game>0)socket.emit("upgrade",{type:getUpgrade(i),game:game})
		}
	}(i))
}
$("#button").on("click",function(){
	if(game>0)socket.emit("click",{game:game});
	
}).bind('keypress', function(e)
{
   if(e.keyCode == 13)
   {
      return false;
   }
});

